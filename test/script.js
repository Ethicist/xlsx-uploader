$(document).ready(function () {
  var table = $("#table").DataTable({
    language: {
      url: '../assets/ru.json'
    },
    ajax: "../data/knigi.json",
    dom:
      "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4 dt-buttons text-center'B><'col-sm-12 col-md-4'f>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-12 col-md-5 entries'i><'col-sm-12 col-md-7'p>>",
    paging: true,
    autoWidth: true,
    buttons: ["excelHtml5", "pdfHtml5", "print"],
    responsive: true,
    destroy: true,
    deferRender: true,
    columns: [
      null,
      null,
      null,
      null,
      {
        "data": undefined,
        "defaultContent": ""
      }
    ]
  });
});
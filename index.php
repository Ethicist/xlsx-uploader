<!DOCTYPE html>
<html lang="ru">

  <head>
    <meta charset="utf-8">
    <title>XLS{X} Uploader</title>
    <meta name="viewport" content="width=device-width,initial-scale=1" />

    <link rel="stylesheet" href="./assets/bootstrap.min.css" />
    <link rel="stylesheet" href="./assets/font.css" />
    <link rel="stylesheet" href="./assets/main.css" />
    <link rel="stylesheet" href="./assets/style.css" />

    <link href="https://www.joomla.org/templates/joomla/images/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180" />
    <link href="https://www.joomla.org/templates/joomla/images/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />
    <link href="https://www.joomla.org/templates/joomla/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144" />
    <link href="https://www.joomla.org/templates/joomla/images/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120" />
    <link href="https://www.joomla.org/templates/joomla/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114" />
    <link href="https://www.joomla.org/templates/joomla/images/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76" />
    <link href="https://www.joomla.org/templates/joomla/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72" />
    <link href="https://www.joomla.org/templates/joomla/images/apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57" />
    <link href="https://www.joomla.org/templates/joomla/images/apple-touch-icon.png" rel="apple-touch-icon" />
    <link href="https://www.joomla.org/templates/joomla/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />

  </head>

  <body>
    
    <?php

    // defined('_JEXEC') or die;
    // # Работаем только в сессии авторизованного админа
    // if (!(JFactory::getUser()->authorise('core.admin')) ||
    //      (JFactory::getUser()->guest)) {
    //   die('Функция доступна только авторизованным администраторам.');
    // }

    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', true);

    function translit($str) {
      $tr = array(
        "А" => "a",  "Б" => "b",   "В" => "v", "Г" => "g",  "Д" => "d",
        "Е" => "e",  "Ё" => "yo",  "Ж" => "j", "З" => "z",  "И" => "i",
        "Й" => "y",  "К" => "k",   "Л" => "l", "М" => "m",  "Н" => "n",
        "О" => "o",  "П" => "p",   "Р" => "r", "С" => "s",  "Т" => "t",
        "У" => "u",  "Ф" => "f",   "Х" => "h", "Ц" => "c",  "Ч" => "ch",
        "Ш" => "sh", "Щ" => "sch", "Ъ" => "",  "Ы" => "yi", "Ь" => "",
        "Э" => "e",  "Ю" => "yu",  "Я" => "ya",

        "а" => "a",  "б" => "b",   "в" => "v",  "г" => "g", "д" => "d",
        "е" => "e",  "ё" => "yo",  "ж" => "j",  "з" => "z", "и" => "i",
        "й" => "y",  "к" => "k",   "л" => "l",  "м" => "m", "н" => "n",
        "о" => "o",  "п" => "p",   "р" => "r",  "с" => "s", "т" => "t",
        "у" => "u",  "ф" => "f",   "х" => "h",  "ц" => "c", "ч" => "ch",
        "ш" => "sh", "щ" => "sch", "ъ" => "y",  "ы" => "y", "ь" => "",
        "э" => "e",  "ю" => "yu",  "я" => "ya",

        " " => "_",  "." => "",    "/" => "_",  "\"" => "", "'" => ""
      );
      return strtolower(str_replace('__', '_', preg_replace("/[^a-zA-Z0-9\s]/",
              "_", strtr(trim($str), $tr))));
    }

    function x_die($case) {
      switch ($case) {
        case 1:
        case 2:
          $error = 'Превышен размер загружаемого файла.';
          break;
        case 3:
          $error = 'Файл был получен только частично.';
          break;
        case 4:
          $error = 'Файл не был загружен.';
          break;
        case 6:
          $error = 'Файл не загружен - отсутствует временная директория.';
          break;
        case 7:
          $error = 'Не удалось записать файл на диск.';
          break;
        case 8:
          $error = 'PHP-расширение остановило загрузку файла.';
          break;
        case 9:
          $error = 'Файл не был загружен - директория не существует.';
          break;
        case 10:
          $error = 'Превышен максимально допустимый размер файла.';
          break;
        case 11:
          $error = 'Данный тип файла запрещен.';
          break;
        case 12:
          $error = 'Ошибка при копировании файла.';
          break;
        case 13:
          $error = 'Функция доступна только авторизованным администраторам.';
          break;
        default:
          $error = 'Файл не был загружен - неизвестная ошибка.';
          break;
      }
      die($error);
    }

    # Загружаем конфиг
    $config = require_once __DIR__ . '/config.php';

    # Проверяем все параметры конфига
    # Количество строк в шапке для удаления
    # Удаляются строки от 0 до N
    if (isset($config['header_rows'])) {
      $n = $config['header_rows'];
    } else {
      $n = 0;
    }

    # Количество экземпляров прайса
    if (isset($config['history_size'])) {
      $history_size = $config['history_size'];
    } else {
      $history_size = 1;
    }

    # Путь куда пишется прайс
    if (isset($config['price_path'])) {
      $price_path = $config['price_path'];
    } else {
      $price_path = __DIR__ . '/data/';
    }

    # Путь куда пишется json
    if (isset($config['database_path'])) {
      $database_path = $config['database_path'];
    } else {
      $database_path = __DIR__ . '/data/';
    }

    # Параметры которые не требуют особой проверки
    $clean_history = $config['clean_history'];
    $keep_datasheet = $config['keep_datasheet'];
    $quiet_mode = $config['quiet_mode'];
    $line_numbers = $config['line_numbers'];
    $petroknigi_fix = $config['line_numbers'];
    
    # Разрешенные расширения файлов
    $allowed = array(
      'xls', 'xlsx'
    );

    # Запрещенные расширения файлов
    $denied = array(
      'phtml', 'php', 'php3', 'php4', 'php5', 'php6', 'php7',
      'phps', 'cgi', 'pl', 'asp', 'aspx', 'shtml', 'shtm',
      'html', 'ini', 'log', 'sh', 'js', 'htm', 'css',
      'sql', 'spl', 'scgi', 'fcgi', 'htaccess', 'htpasswd',
      'db', 'iso', 'tar', 'zip', 'gz', 'gitignore'
    );

    if (isset($_FILES['price'])) {

      # Получаем расширение файла
      $basename = $_FILES['price']['name'];
      $filename = pathinfo($basename, PATHINFO_FILENAME);
      $extension = pathinfo($basename, PATHINFO_EXTENSION);

      # Проверка на ошибки при загрузке
      if (!empty($_FILES['price']['error']) ||
           empty($_FILES['price']['tmp_name'])) {
        x_die($_FILES['price']['error']);
      }

      # Проверка по допустимым расширениям файлов
      if (empty($filename) || empty($extension)) {
        x_die(11);
      }
      if (!in_array(strtolower($extension), $allowed)) {
        x_die(11);
      } else if (in_array(strtolower($extension), $denied)) {
        x_die(11);
      } else {

        # DataTables требует только такой конструкции:
        $data = array("data" => []);

        # Если в форме указаны параметры, используем их
        if (isset($_REQUEST['header_rows'])) {
          $n = $_REQUEST['header_rows'];
        }
        if (isset($_REQUEST['database_path'])) {
          $database_path = $_REQUEST['database_path'];
        }
        if (isset($_REQUEST['price_path'])) {
          $price_path = $_REQUEST['price_path'];
        }

        # Проверим директории для загрузки.
        if (!is_dir($database_path)) {
          mkdir($database_path, 0777, true);
        }
        if (!is_dir($price_path)) {
          mkdir($price_path, 0777, true);
        }

        # Работаем с XLS
        if (strtolower($extension) == 'xls') {

          # Подключаем класс SimpleXLS
          require_once __DIR__ . '/lib/SimpleXLS.php';

          if ($xls = SimpleXLS::parse($_FILES['price']['tmp_name'])) {

            # Считаем максимальное количество столбцов
            $cols = 0;
            foreach ($xls->rows() as $row) {
              $r_length = count($row);
              if ($cols < $r_length) {
                $cols = $r_length;
              }
            }
            
            if ($line_numbers) {
              $row_index = 1;
            }

            # Проход по строкам
            foreach ($xls->rows() as $key => $row) {

              # Пропускаем строки которые нужно обрезать
              if ($key < $n) {
                continue;
              }

              $row_tmp_data = array();
              
              if ($line_numbers) {
                $row_tmp_data[] = $row_index;
              }

              # Проход по столбцам
              for ($i = 0; $i < $cols; $i ++) {
                if (isset($row[$i]) && !empty($row[$i])) {
                  $row_tmp_data[] = $row[$i];
                } else {
                  if($petroknigi_fix) {
                    if($i == 0) {
                      $row_tmp_data[] = ' ';
                    }
                  }
                }
              }
              $data["data"][] = $row_tmp_data;
              
              if ($line_numbers) {
                $row_index++;
              }
            }
          } else {
            echo SimpleXLS::parseError();
          }
        } else if (strtolower($extension) == 'xlsx') {

          # Работаем с XLSX
          # Подключаем класс SimpleXLSX
          require_once __DIR__ . '/lib/SimpleXLSX.php';

          if ($xlsx = SimpleXLSX::parse($_FILES['price']['tmp_name'])) {

            # Здесь свой метод подсчёта размеров таблицы
            $dim = $xlsx->dimension();
            $cols = $dim[0];
            
            if ($line_numbers) {
              $row_index = 1;
            }
            
            # Проход по строкам
            foreach ($xlsx->rows() as $key => $row) {
              
              # Пропускаем строки которые нужно обрезать
              if ($key < $n) {
                continue;
              }

              $row_tmp_data = array();
              
              if ($line_numbers) {
                $row_tmp_data[] = $row_index;
              }
              
              # Проход по столбцам
              for ($i = 0; $i < $cols+1; $i ++) {
                if (isset($row[$i]) && !empty($row[$i])) {
                  $row_tmp_data[] = $row[$i];
                } else {
                  if($petroknigi_fix) {
                    if($i == 0) {
                      $row_tmp_data[] = ' ';
                    }
                  }
                }
              }
              $data["data"][] = $row_tmp_data;
              
              if ($line_numbers) {
                $row_index++;
              }
            }
          } else {
            echo SimpleXLSX::parseError();
          }
        }

        # Пишем JSON по указанному пути
        if (isset($_REQUEST['json_name']) && !empty($_REQUEST['json_name'])) {
          $json_filename = translit($_REQUEST['json_name']);
        } else {
          $json_filename = date("Y-m-d_") . translit($filename);
        }
        $json_out = $database_path . $json_filename . '.json';
        $json_data = json_encode($data, JSON_UNESCAPED_UNICODE);
        if (!file_put_contents($json_out, $json_data)) {
          x_die(0xdeadbabe);
        } else {
          if (isset($quiet_mode) && $quiet_mode == false) {
            echo '<script>' .
                 'console.log(\'' . 'Загружен: ' . $json_out . '\');' .
                 '</script>';
          }
        }

        # Если параметр сохранения прайса не изменён
        if (!isset($keep_datasheet) || $keep_datasheet == true) {
          # Сохраняем файл прайса
          if (isset($_REQUEST['price_name']) && !empty($_REQUEST['price_name'])) {
            $price_filename = translit($_REQUEST['price_name']);
          } else {
            $price_filename = date("Y-m-d_") . translit($filename);
          }
          $price_out = $price_path . $price_filename . '.' . $extension;
          $price_data = $_FILES['price']['tmp_name'];
          if (!move_uploaded_file($price_data, $price_out)) {
            x_die(0xdeadbabe);
          } else {
            if (isset($quiet_mode) && $quiet_mode == false) {
              echo '<script>' .
                   'console.log(\'' . 'Загружен: ' . $price_out . '\');' .
                   '</script>';
            }
          }
        }

        # Удаление предыдущих версий файлов
        if (isset($clean_history) && $clean_history == true) {
          # Получаем все JSON-файлы из директории
          $db_json_content = glob("$database_path*.{json,JSON}", GLOB_BRACE);

          # Сортируем по дате их загрузки на сервер
          usort($db_json_content, function($a, $b) {
            return filemtime($b) - filemtime($a);
          });
          //$db_json_content = array_reverse($db_json_content);
          # Удаляем последние записи
          foreach ($db_json_content as $db_file) {
            $index = array_search($db_file, $db_json_content);
            if ($index > $history_size) {
              if (isset($quiet_mode) && $quiet_mode == false) {
                echo '<script>' .
                     'console.log(\'' . 'Удалён: ' . $db_file . '\');' .
                     '</script>';
              }
              unlink($db_file);
            }
          }

          # Получаем все файлы прайсов
          $price_content = glob("$price_path*.{xls,xlsx,XLS,XLSX}", GLOB_BRACE);

          # Сортируем по дате их загрузки на сервер
          usort($price_content, function($a, $b) {
            return filemtime($b) - filemtime($a);
          });
          //$price_content = array_reverse($price_content);
          # Удаляем последние записи
          foreach ($price_content as $price_file) {
            $index = array_search($price_file, $price_content);
            if ($index > $history_size) {
              if (isset($quiet_mode) && $quiet_mode == false) {
                echo '<script>' .
                     'console.log(\'' . 'Удалён: ' . $price_file . '\');' .
                     '</script>';
              }
              unlink($price_file);
            }
          }
        }
      }
    }
    ?>

    <div class="container" role="main">
      <h1>
        <a href="./">
          <svg xmlns="http://www.w3.org/2000/svg"
               viewBox="0 0 512 512" width="40" height="40"
               style="position:relative;top:-2px;">
          <path fill="#7ac043" d="M26.667 87.799c0-34.185 27.511-61.694 61.388-61.694 30.797 0 55.947 22.48 60.771 51.532 33.467-7.802 68.882.615 99.063 30.797l-45.476 45.476c-21.044-21.044-43.73-16.732-56.872-3.593-14.679 14.681-14.679 38.906 0 53.586l102.142 102.143-45.167 45.475c-90.029-89.516-51.02-51.019-102.45-102.346-27.512-27.204-35.93-66.521-25.458-101.526-27.615-5.956-47.941-30.49-47.941-59.85z"/>
          <path fill="#f9ae41" d="M159.607 207.29l45.476 45.478c10.266-10.267 92.082-92.082 102.347-102.451 14.681-14.679 38.6-14.679 53.278 0 13.14 13.14 17.451 35.931-3.592 56.871l45.167 45.477c32.028-32.028 39.523-69.396 29.668-103.887 29.975-4.21 53.277-29.977 53.277-61.081 0-34.082-27.511-61.695-61.388-61.695-31.105 0-56.871 23.097-61.079 52.97-34.697-10.163-73.604-1.539-100.91 25.767-18.786 19.607-72.989 73.398-102.244 102.551z"/>
          <path fill="#ee4035" d="M432.979 363.531c8.417-33.568-.925-70.319-26.999-96.395-12.113-12.522 5.132 4.825-102.144-102.346l-45.475 45.477 102.347 102.346c14.681 14.682 14.681 38.6 0 53.278-13.14 13.14-35.929 17.452-56.872-3.592l-45.167 45.476c28.334 31.001 69.805 39.831 105.427 28.743 5.646 28.129 30.489 49.378 60.465 49.378 33.877 0 61.387-27.512 61.387-61.694-.001-31.005-23.097-56.462-52.969-60.671z"/>
          <path fill="#4f91cd" d="M346.438 309.02l-45.168-45.475c-89.31 88.693-51.738 51.737-102.348 102.45-14.679 14.679-38.598 14.679-53.277 0-13.449-13.757-17.349-36.238 3.286-56.871l-45.169-45.476C72.76 294.65 64.753 330.579 73.479 364.56c-27.408 6.158-47.426 30.692-47.426 59.743 0 34.186 27.511 61.696 61.388 61.696 29.359 0 53.894-20.326 60.155-47.94 33.568 8.417 70.319-.615 96.702-26.689 32.951-32.851 12.522-12.731 102.14-102.35z"/>
          </svg>
          XLS{X} Uploader
        </a>
      </h1>

      <div class="card">

        <form method="post" enctype="multipart/form-data">

          <div class="card-header">
            <div class="row">

              <input type="file"
                     name="price"
                     class="input-file"
                     required
                     >

              <div class="input-group col-md-12 file-uploader">
                <span class="input-group-addon">
                  <button class="select-btn btn btn-primary" type="button">
                    Выбрать файл
                  </button>
                </span>

                <input type="text"
                       class="form-control"
                       id="frontFileName"
                       placeholder="Файл не выбран"
                       required
                       disabled
                       >

                <span class="input-group-btn">
                  <button class="reset-btn btn btn-light"
                          type="button"
                          title="Сбросить"
                          disabled>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                      <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                    </svg>
                  </button>
                </span>
              </div> <!-- ./input-group -->
            </div> <!-- ./row -->
          </div> <!-- ./card-header -->

          <div class="card-body">

            <div class="form-group row">
              <label class="col-md-4 col-form-label">
                Строк в шапке таблицы
              </label>

              <div class="col-md-8">
                <div class="input-group">

                  <input type="number"
                         name="header_rows"
                         class="form-control"
                         value="<?php echo $config['header_rows']; ?>"
                         />

                  <!--div class="input-group-append">
                    <a class="input-group-text">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-question-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
                      </svg>
                    </a>
                  </div-->
                  
                </div> <!-- ./input-group -->

                <span class="form-text text-muted">
                  <small>
                    <i>
                      Количество строк, удаляемых из шапки документа.
                    </i>
                  </small>
                </span>
              </div> <!-- ./col-md-8 -->
            </div> <!-- ./row -->

            <hr/>

            <div class="form-group row">
              <label class="col-md-4 col-form-label">
                Загрузить прайс
              </label>

              <div class="col-md-8">
                
                <?php
                  if (empty($config['price_path']) ||
                     !isset($config['price_path'])) {
                ?>
                
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"> в </div>
                  </div>
                  
                  <input type="text"
                         name="price_path"
                         required="required"
                         class="form-control"
                         value="<?php echo $price_path; ?>"
                         />
                    
                  <!--div class="input-group-append">
                    <a class="input-group-text">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-question-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
                      </svg>
                    </a>
                  </div-->
                  
                </div>  <!-- ./input-group -->
                
                <?php } ?>

                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"> как </div>
                  </div>

                  <input type="text"
                         name="price_name"
                         required="required"
                         class="form-control"
                         value=""
                         />

                  <!--div class="input-group-append">
                    <a class="input-group-text">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-question-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
                      </svg>
                    </a>
                  </div-->
                  
                </div> <!-- ./input-group -->

                <span id="json-pathHelpBlock" class="form-text text-muted">
                  <small>
                    <i>
                      Имя файла без расширения. Транслитерируется.
                    </i>
                  </small>
                </span>
              </div> <!-- ./col-md-8 -->
            </div> <!-- ./row -->

            <hr>

            <div class="form-group row">
              <label class="col-md-4 col-form-label">
                Сохранить JSON
              </label>

              <div class="col-md-8">
                
                <?php
                  if (empty($config['database_path']) ||
                   !isset($config['database_path'])) {
                ?>
                
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"> в </div>
                  </div>

                  <input type="text"
                         name="database_path"
                         required="required"
                         class="form-control"
                         value="<?php echo $database_path; ?>"
                         />

                  <!--div class="input-group-append">
                    <a class="input-group-text">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-question-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
                      </svg>
                    </a>
                  </div-->
                  
                </div> <!-- ./input-group -->
                
                <?php } ?>

                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text"> как </div>
                  </div>

                  <input type="text"
                         name="json_name"
                         required="required"
                         class="form-control"
                         value=""
                         />

                  <!--div class="input-group-append">
                    <a class="input-group-text">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-question-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z"/>
                      </svg>
                    </a>
                  </div-->
                  
                </div> <!-- ./input-group -->

                <span id="json-pathHelpBlock" class="form-text text-muted">
                  <small>
                    <i>
                      Имя файла без расширения. Транслитерируется.
                    </i>
                  </small>
                </span>
              </div> <!-- ./col-md-8 -->
            </div> <!-- ./row -->
          </div> <!-- ./card-body -->

          <div class="card-footer">
            <div class="form-group row">
              <div class="col-md-12">
                <button name="submit"
                        type="submit"
                        class="btn btn-dark"
                        id="upload-file"
                        disabled >
                  Загрузить
                </button>
              </div>
            </div>
          </div> <!-- ./card-footer -->

        </form>
      </div> <!-- ./card -->

      <footer>
        <div class="bottom">
          <div class="text-left" style="padding-left: 15px;">
            <p>
              &copy;
              <?php
                echo (date('Y') != '2021') ? date('Y') . '&mdash;2021'  : '2021'
              ?>
              <a href="https://ethicist.site/">Ethicist</a>,
              <a href="https://choosealicense.com/licenses/mit/">MIT License</a>
            </p>
          </div>
          <a class="text-right"
             href="/administrator/"
             target="_blank"
             title="Панель управления"
             style="padding-right: 15px;"
          >
            <i class="fab fa-joomla"></i>
          </a>
        </div>
      </footer>

    </div> <!-- ./container -->

    <script>
      function addEventListener(el, eventName, handler) {
        if (el.addEventListener) {
          el.addEventListener(eventName, handler);
        } else {
          el.attachEvent('on' + eventName, function() {
            handler.call(el);
          });
        }
      }

      var selectButton = document.querySelector('.select-btn');
      var resetButton = document.querySelector('.reset-btn');
      var submitButton = document.getElementById('upload-file');
      var hiddenField = document.querySelector('.input-file');
      var frontField = document.getElementById('frontFileName');

      frontField.value = '';
      submitButton.disabled = 'disabled';
      resetButton.disabled = 'disabled';

      addEventListener(selectButton, 'click', function() {
        hiddenField.click();
      });

      addEventListener(resetButton, 'click', function() {
        hiddenField.value = '';
        frontField.value = '';
        submitButton.disabled = 'disabled';
        resetButton.disabled = 'disabled';
      });

      addEventListener(hiddenField, 'change', function() {
        var value = hiddenField.value.replace(/C:\\fakepath\\/i, '');
        frontField.value = value;
        submitButton.disabled = false;
        resetButton.disabled = false;
      });
    </script>

  </body>

</html>